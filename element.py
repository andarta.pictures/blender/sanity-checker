import bpy

class SANITY_ELEMENT:
    def __init__(self):
        self.errors = []
        self.msg = ''     
        self.fixable = False  
        self.fixed = False
        self.parent_checks = []

    def check(self):
        pass
    def fix(self):
        pass
    
    def parents_are_ok(self):
        for cp_name in self.parent_checks :
            parent = [cp for cp in bpy.context.scene.sanity_check_props.checkpoints if cp.__class__.__name__ == cp_name]
            if len(parent) == 1 :
                cp = parent[0]
                if len(cp.errors) != 0 :
                    return False
            else : return False
        return True

    def get_fix_msg(self):
        return 'FIX ' + str(len(self.errors)) + '  errors from ' + self.__class__.__name__
    
    def report(self):
        errors = []
        for err in self.errors :
            errors.append(self.get_report_msg(err))
        return errors
    
    def get_report_msg(self, err):
        return self.msg + str(err)
    


class SC_EXAMPLE(SANITY_ELEMENT):
    #Template for sanity check element (checkpoint)
    def __init__(self):
        super().__init__()
        self.msg = 'BAD NAMING (A instead of Z)' #default sanity report message is this string followed by str(error) for error in self.errors
        self.fixable = True #set this to True if there is a fix() method to fix errors       
        self.parent_checks = ['SC_COL_GP']  # list here parent checkpoint class name that have to return no errors for running this checkpoint 
    def check(self): 
        #check for errors, if found, append anything you will need to fix to self.errors (at least None)
        for ob in bpy.context.scene.objects:
            if 'A' in ob.name :
                self.errors.append((ob, ob.name.replace('A','Z')))
    def fix(self): 
        #from what is stored in self.errors, fix the error in this method
        for ob, name in self.errors :
            ob.name = name    
    def get_fix_msg(self): 
        #customize the fixer button message here (return a string) - delete to use default message
        return 'Rename ' + str(len(self.errors)) + '  objects'    
    def get_report_msg(self, err):
        #customize the sanity report messages here (return a list of strings) - delete to use default message
        return self.msg + ' : ' + str(err)