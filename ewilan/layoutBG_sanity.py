import bpy
from ..element import SANITY_ELEMENT
from .sanity_functions import *
import gazu
from math import pi

def get_kitsu_shot(ep_sq_sh):
    ep_name, sq_name, sh_name = ep_sq_sh.split('_')
    project = gazu.project.get_project_by_name("EWILAN") 
    episode = gazu.shot.get_episode_by_name(project, ep_name)
    sequence = gazu.shot.get_sequence_by_name(project,sq_name,episode)
    shot = gazu.shot.get_shot_by_name(sequence, sh_name)
    return shot
    

def get_linked_shots():
    filename = os.path.basename(bpy.data.filepath)
    if len(filename.split('_')) == 6 :
        proj_code, ep, sq, sh, task, worker = filename.split("_")
    elif len(filename.split('_')) == 5 :
        proj_code, ep, sq, sh, task = filename.split("_")
    
    project = gazu.project.get_project_by_name("EWILAN")
    episode = gazu.shot.get_episode_by_name(project, ep)
    sequence = gazu.shot.get_sequence_by_name(project,sq,episode)
    shot = gazu.shot.get_shot_by_name(sequence, sh)
    #print(episode)
    linked_shots = ['_'.join(filename.split("_")[2:4])] #shot from filename
    for shot in gazu.shot.all_shots_for_episode(episode):
        metadata = shot['data']
        if metadata and 'use_bg_from' in metadata.keys():
            use_bg_from = metadata['use_bg_from']
            if isinstance(use_bg_from, list) and use_bg_from != []:
                use_bg_from = use_bg_from[0]
            if use_bg_from == '_'.join([ep,sq,sh]):
                sh_seq = gazu.shot.get_sequence_from_shot(shot)
                linked_shots.append(sh_seq['name'] +'_' +shot['name'])
    return linked_shots

class SC_SCN_BUILD(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Cannot find SCN_BUILD scene'        
    def check(self): 
        if not 'SCN_BUILD' in [s.name for s in bpy.data.scenes] :
            self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg


class SC_CAMS_FOR_SHOTS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_KITSU_CONNECTED', 'SC_CONFIRM_LAYOUT_SHOT', 'SC_SCN_BUILD']   
        self.msg = 'Shot camera : ' 
    def check(self): 
        linked_shots = get_linked_shots()
        #print(linked_shots)
        for ob in bpy.data.scenes['SCN_BUILD'].objects :
            if ob.type == 'CAMERA' :
                if ob.name[4:] not in linked_shots and ob.name[4:] not in [l + '_OLD' for l in linked_shots] :
                    #print('######', 'CAM_' + ob.name[4:])
                    self.errors.append((ob.name, 'INVALID_NAME'))
        for sh_name in linked_shots :
            if sh_name not in [ob.name[4:] for ob in bpy.data.scenes['SCN_BUILD'].objects if ob.type == 'CAMERA']:
                self.errors.append(('CAM_' + sh_name, 'MISSING'))   
    def get_report_msg(self, err):
        match err[1] :
            case 'INVALID_NAME':
                return self.msg + 'Bad naming of shot camera : ' + err[0]
            case 'MISSING':
                return self.msg + 'Cannot find shot camera : ' + err[0]

class SC_CAMS_ROTATION(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        #self.parent_checks = ['SC_CAMS_FOR_SHOTS']   
        self.msg = 'Rotated camera (only Y axis rotation is allowed) : ' 
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.type == 'CAMERA' :
                if not (round(ob.rotation_euler[0], 6) == round(pi/2, 6) and round(ob.rotation_euler[2],6) == 0):
                    self.errors.append(ob)
                if ob.animation_data and ob.animation_data.action :
                    for fc in ob.animation_data.action.fcurves :
                        if fc.data_path == 'rotation_euler' and fc.array_index in [0,2]:
                            for kf in fc.keyframe_points : 
                                match fc.array_index :
                                    case 0 :
                                        rot = round(pi/2, 6)
                                    case 2 :
                                        rot = 0
                                if round(kf.co.y, 6) != rot and ob not in self.errors :
                                    self.errors.append(ob)
                                    break
    def get_report_msg(self, err):
        return self.msg + err.name

class SC_OBJ_NO_ANIM(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_CAMS_FOR_SHOTS']   
        self.msg = 'Animation (action) found on ' 
        self.fixable = True
    def check(self): 
        for scene in bpy.data.scenes :
            for ob in scene.objects :
                if not any([ob.type == 'CAMERA', ob.type == 'EMPTY' and 'CTRL_DOF' in ob.name]) and ob.animation_data and ob.animation_data.action :
                    self.errors.append(ob)             
    def get_report_msg(self, err):
        return self.msg + err.name + ' (animation is allowed for cameras only)'      
    def fix(self) :
        for ob in self.errors :
            ob.animation_data_clear()
    def get_fix_msg(self): 
        #customize the fixer button message here (return a string) - delete to use default message
        return 'Clear anim data on  ' + str(len(self.errors)) + '  objects'      

class SC_SCNS_FOR_SHOTS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_KITSU_CONNECTED', 'SC_CONFIRM_LAYOUT_SHOT']   
        self.msg = 'Shot scene : ' 
    def check(self): 
        linked_shots = get_linked_shots()
        for scene in bpy.data.scenes :
                if scene.name[4:] not in linked_shots and scene.name != 'SCN_BUILD' :
                    self.errors.append((scene.name, 'INVALID_NAME'))
        if len(linked_shots) > 1:
            for sh_name in linked_shots :
                if sh_name not in [scene.name[4:] for scene in bpy.data.scenes]:
                    self.errors.append(('SCN_' + sh_name, 'MISSING'))   
    def get_report_msg(self, err):
        match err[1] :
            case 'INVALID_NAME':
                return self.msg + 'Bad naming of shot scene : ' + err[0]
            case 'MISSING':
                return self.msg + 'Cannot find shot scene : ' + err[0]
            
class SC_OUTPUT_RES(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Bad output resolution, should be 1920 * 816, found ' 
        self.fixable = True       
    def check(self):
        for scene in bpy.data.scenes :
            if not (scene.render.resolution_x, scene.render.resolution_y) == (1920,816):
                self.errors.append([scene, (scene.render.resolution_x, scene.render.resolution_y)])
    def get_report_msg(self, err):
        return self.msg + str(err[1]) + 'on scene ' + err[0].name
    def fix(self) :
        for scn, res in self.errors :
            scn.render.resolution_x = 1920
            scn.render.resolution_y = 816
    def get_fix_msg(self): 
        #customize the fixer button message here (return a string) - delete to use default message
        return 'Adjust resolution of  ' + str(len(self.errors)) + '  scenes'    

class SC_SCN_LENGTH(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Incorrect scene length (according to kitsu) :  ' 
        self.fixable = True     
        self.parent_checks = ['SC_SCNS_FOR_SHOTS']  
    def check(self):
        #print('SCENE LENGTH')
        check_scenes = []
        ep = os.path.basename(bpy.data.filepath).split('.')[0].split('_')[1]
        max_length = 0
        if len(bpy.data.scenes) > 1 : #splitted scenes  
            for scene in [s for s in bpy.data.scenes if s.name != 'SCN_BUILD']:
                ep_sq_sh =  scene.name.replace('SCN',ep)
                shot = get_kitsu_shot(ep_sq_sh)
                if shot['nb_frames'] > max_length :
                    max_length = shot['nb_frames']
                check_scenes.append((scene, shot['nb_frames']))
            check_scenes.append((bpy.data.scenes['SCN_BUILD'], max_length))
        else : #unsplitted
            ep_sq_sh = '_'.join(os.path.basename(bpy.data.filepath).split('.')[0].split('_')[1:4])
            shot = get_kitsu_shot(ep_sq_sh)
            check_scenes.append((bpy.data.scenes['SCN_BUILD'], shot['nb_frames']))
        for scn, length in check_scenes:
            if scn.frame_end != length:
                self.errors.append((scn, length))
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' should be ' + str(err[1])
    def fix(self) :
        for scn, length in self.errors:
            scn.frame_end = length
    def get_fix_msg(self): 
        #customize the fixer button message here (return a string) - delete to use default message
        return 'Adjust length of  ' + str(len(self.errors)) + '  scenes according to kitsu'    
    
class SC_COL_CAM(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Cant find collection ' 
        self.parent_checks = ['SC_SCNS_FOR_SHOTS', 'SC_SCN_BUILD']       
    def check(self):
        for scene in bpy.data.scenes:
            if scene.name.replace('SCN', 'COL-RDR') not in scene.collection.children.keys() :
                self.errors.append((scene.name.replace('SCN', 'COL-RDR'), scene.name))
            else :
                if scene.name.replace('SCN', 'COL-RDR_CAM') not in scene.collection.children[scene.name.replace('SCN', 'COL-RDR')].children.keys() :
                    self.errors.append((scene.name.replace('SCN', 'COL-RDR_CAM'), scene.name))
    def get_report_msg(self, err):
        return self.msg + str(err[0]) + ' in scene ' + err[1]
    
class SC_CAM_OBJS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Cam object error :  '     
        self.parent_checks = ['SC_COL_CAM']  
    def check(self):
        cols_to_check = []
        if len(bpy.data.scenes) > 1 : #splitted scenes  
            for scene in [s for s in bpy.data.scenes if s.name != 'SCN_BUILD']:
                col_cam = bpy.data.collections[scene.name.replace('SCN', 'COL-RDR_CAM')]
                cols_to_check.append((col_cam, scene.name.replace('SCN_', '')))
        else : #unsplitted
            cols_to_check.append((bpy.data.collections['COL-RDR_CAM_BUILD'], '_'.join(os.path.basename(bpy.data.filepath).split('.')[0].split('_')[2:4])))
        
        for col, sq_sh in cols_to_check:
            for ob in col.objects:
                if ob.type == 'CAMERA' :
                    if not (ob.name == 'CAM_' + sq_sh or ob.name == 'CAM_' + sq_sh + '_OLD') :
                        self.errors.append((ob.name, ' : incorrect naming' ))  
                elif ob.type == 'EMPTY' :
                    if not (ob.name == 'CTRL_DOF_' + sq_sh or ob.name == 'CTRL_DOF_' + sq_sh + '_OLD') :
                        self.errors.append((ob.name, ' : incorrect naming' ))
                
                elif ob.type == 'CURVE' :
                    if not (ob.name == 'CTRL_CAM_' + sq_sh or ob.name == 'CTRL_CAM_' + sq_sh + '_OLD') :
                        self.errors.append((ob.name, ' : incorrect naming' ))
                else : 
                    self.errors.append((ob.name, 'has invalid type (only camera / empty / curve are allowed)'))

                if ob.name.endswith('OLD'):
                    self.errors.append((ob.name, 'is suffixed OLD and should be deleted' ))
                #else :
                #    if not ob.library :
                #        self.errors.append((ob.name, 'is not linked (that should be done at LAYPOS beginning)'))

    def get_report_msg(self, err):
            return self.msg + err[0] + ' ' + err[1]

class SC_DOF_GP_MODIFIER(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Missing blur effect (dof activated) on object ' 
        self.fixable = True       
    def check(self):
        scenes = []
        if len(bpy.data.scenes) > 1 : #splitted scenes  
            for scene in [s for s in bpy.data.scenes if s.name != 'SCN_BUILD']:
                scenes.append(scene)
        else : 
            scenes = [bpy.data.scenes[0]]
        for scene in scenes :
            for cam in scene.collection.all_objects :
                if cam.type == 'CAMERA':
                    if cam.data.dof.use_dof :
                        for col in [c for c in scene.collection.children_recursive if (c.name.startswith('COL_BG')  and c.name.endswith('CLEAN')) or c.name.startswith('COL_PROPS-BG_CLEAN')]:
                            for ob in col.all_objects :
                                found_fx = False
                                fx_uses_dof = False
                                for fx in ob.shader_effects :
                                    if fx.type == 'FX_BLUR':
                                        found_fx = True
                                        if fx.use_dof_mode :
                                            fx_uses_dof = True
                                        break
                                if not found_fx :
                                    self.errors.append((ob, 'missing'))
                                elif not fx_uses_dof :
                                    self.errors.append((ob, 'use dof unchecked'))
 
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' : ' + err[1]
    def fix(self) :
        for ob, errtype in self.errors :
            if errtype == 'missing' :
                fx = ob.shader_effects.new('Blur','FX_BLUR')
                fx.use_dof_mode = True
            else :
                for fx in ob.shader_effects :
                    if fx.type == 'FX_BLUR':
                        fx.use_dof_mode = True
                        break
    def get_fix_msg(self): 
        #customize the fixer button message here (return a string) - delete to use default message
        return 'Add/fix DOF blur effect on   ' + str(len(self.errors)) + '  objects'    


class SC_OBJ_NAMING(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Invalid object name : ' 
    def check(self):
        for col in [c for c in bpy.data.collections if any([item in c.name for item in ['COL_BG', 'COL_PROPS-BG']])]:
            for ob in col.objects :
                if not ob.library :
                    if '.' in ob.name or ' ' in ob.name :
                        self.errors.append((ob, 'Found invalid character (space or .)'))
                    found_lvl = False
                    for part in ob.name.split('_') :
                        if part.startswith('LVL') :
                            found_lvl = True 
                            if any([not len(part) == 6]):
                                self.errors.append((ob,'Found something weird in LVL part'))
                    if not found_lvl :
                        self.errors.append((ob, 'No LVL part found'))
                user_scene = ob.users_scene[0]
                scene_tag = user_scene.name[4:]
                if not scene_tag in ob.name :
                    self.errors.append((ob, f'Expected to find {scene_tag} in the name'))
    def get_report_msg(self, err):
        return self.msg + err[0].name +' => '+ err[1]

class SC_BG_MATERIALS_BLENDING(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()          
        self.msg = 'Incorrect shader blending (should be "alpha hashed"): ' 
        self.fixable = True       
    def check(self):
        for material in bpy.data.materials :
            if not material.is_grease_pencil and 'BG' in material.name:
                if material.blend_method != 'HASHED' :
                    self.errors.append(material)
    def get_report_msg(self, err):
        return self.msg + err.name 
    def fix(self) :
        for material in self.errors :
            material.blend_method = 'HASHED'
    def get_fix_msg(self): 
        return 'Change blend method on ' + str(len(self.errors)) + '  materials'  





#//DONE shader des bg plates en alpha hashed et non alpha blend ? sq22_sh3010

#//DONE exception si clé d'animation su CTRL_DOF

#//DONE CHECKER LVL010 (3 chiffres apres) dans le nom des GP de COL_clean

#//DONE CHECKER si les grease pencil on dans leur nom le tag de leur scene (casse le split scene si err dans build)





#CTRL DOF : optionnel, porte le nom du shot, dans col build et dans col scene (si split)
#CTRL CAM idem

# CAM OBJECTS :-doit être linké

#DOF enabled : effet blur activé sur les grease pencils du shot

##SCN_BUILD : durée = la scene la plus longue------------- DONE
            
## SCN SHOT : durée du shot depuis kitsu_______DONE

#COLLECTIONS : COL-GP :
#                   COL_BG_BUILD_ROUGH : jaune
#                   COL_BG_BUILD_CLEAN : jaune
#                   COL_BG_BUILD_PAINT : jaune
#                   COL_PROPS_BG_BUILD_ROUGH : vert
#                   COL_PROPS_BG_BUILD_ROUGH : vert
#                   COL_PROPS_BG_BUILD_ROUGH : vert
#           COL-UTILS
#           COL-REFS
#           COL-RDR_CAM_BUILD/SH00x 
                    #CAM_SQ00X_SH00X et CTRL_DOF_SQ00X_SH00X

# collection : ne pas masquer avec l'oeil mais utiliser la checkbox

# aucun datablock nommé .00x

#SCN_BUILD
# dans COL CLEAN : uniquement des layers GLPYR_STROKE et GPLYR_FILL

#max 250k points dans COL_CLEAN

#2 pt strokes

#cohérence Yloc et suffixe nom (LVL_010 devant tout)
#GPSH + nom de l'obj dans la scene build
# dans les scenes de shot : objets = Full copies / GPSH = linked copy ? voir avec lyonel et pierre

#ECHELLE du line up (pas trop de scale down (limit 0.5)) attention à o.empty_display_size