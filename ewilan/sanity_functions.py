import bpy 
import os

def get_blender_asset_name():
    return '-'.join(os.path.basename(bpy.data.filepath).split('_')[3:-2])

def get_col_gp():
    if 'COL-GP' in bpy.context.scene.collection.children.keys() :
        return bpy.context.scene.collection.children['COL-GP']
    return None

def get_col_rig():
    col_gp = get_col_gp()
    col_rig_name = 'COL_RIG_'+ get_blender_asset_name()
    if col_rig_name in bpy.data.collections.keys() :
        return bpy.data.collections[col_rig_name]
    #colls = [col for col in col_gp.children if col.name.startswith('COL_RIG') and len(col.name.split('_')) == 3]
    #if len(colls) == 1 :
        #return colls[0]
    return None

def get_asset_name(): #deprecated to filename split in get_blender_asset_name()
    col_rig = get_col_rig()
    if col_rig :
        return col_rig.name.split('_')[2]
    return None

def get_asset_empty_col():
    for col in bpy.context.scene.collection.children :
        if col.name.startswith('Asset_') and col.name.endswith('_Collection') :
            return col
    return None

def get_rig_master():
    col_rig = get_col_rig()
    asset_name = get_asset_name()
    if 'CTLR_RIG_' + asset_name + '_MASTER_C' in col_rig.objects.keys() :
        return bpy.data.objects['CTLR_RIG_' + asset_name + '_MASTER_C']
    return None

def get_now_kf(current_frame, fcurve):
    '''returns keyframe at current_frame, if it exists
    args:
        current_frame (int): the frame to look for
        fcurve (fcurve): the fcurve to look in'''
    if fcurve is not None :
        for keyframe in fcurve.keyframe_points : 
            if keyframe.co.x == current_frame :
                return keyframe
    return None

def get_now_gpframe(current_frame, layer):
    for frame in layer.frames :
        if frame.frame_number == current_frame :
            return frame
    return None

def list_to_str(list) :
    string = ''
    for i, item in enumerate(list) :
        if i > 0 :
            string = string + '&&'
        string = string + str(item)
    return string

def str_to_list(string):
    return string.split('&&')

def find_kf(fc,t):
    for kf in fc.keyframe_points :
        if kf.co[0] == t :
            return True
    return False

def find_gpkf(layer, t) :
    for frame in layer.frames :
        if frame.frame_number == t :
            return True
    return False

def is_valid_gpm(gpm):
    if gpm.src_ob and len(gpm.layers) > 0 :
        return True
    else :
        return
def update_points_on_gp(ob) :
    if ob.type == 'GPENCIL' :
        for layer in ob.data.layers :
            for frame in layer.frames :
                for stroke in frame.strokes :
                    stroke.points.update() 

def force_constant_kf(ob):
    if ob.animation_data and ob.animation_data.action :
        for fc in ob.animation_data.action.fcurves :
            for kf in fc.keyframe_points :
                kf.interpolation = 'CONSTANT'

def has_enforced_kf(import_data, col_rig):
    root_objs = [asr.object for asr in import_data.id_data.asset_root_objects]
    for ob in get_objects_and_childs(root_objs, col_rig.all_objects) :
        if ob.animation_data and ob.animation_data.action :
            for fc in ob.animation_data.action.fcurves :
                for t in range(import_data.range[0], import_data.range[1]+1):
                    if not find_kf(fc,t) :
                        return False
        if ob.type == 'GPENCIL' :
            for layer in ob.data.layers :
                for t in range(import_data.range[0], import_data.range[1]+1):
                    if not find_gpkf(layer, t) :
                        return False           
    return True

def get_objects_and_childs(objects, objects_list):
    '''returns a list of all the objects in the dependancy tree (childrens, 'child of' constraints targeting the object)
    args : objects = the objects you wan't to get childrens from
           objects_list = the list where you want to look for children (basically bpy.context.scene)'''
    result = []
    for ob in objects :
        if ob not in result :
            result.append(ob)
        for child in get_constraint_children_recursive(ob, objects_list, []) :
            if child not in result :
                result.append(child)
    return result

def get_constraint_children_recursive(object, objects_list, ctrl_list=[]):
    '''
    returns a list of all the objects in the dependancy tree (childrens, 'child of' constraints targeting the object) recursively
    args : object = the object you wan't to get childrens from
           objects_list = the list where you want to look for children (basically bpy.context.scene)
           ctrl_list = provide an empty list, this is to avoid infinite recursions
    '''
    childs = [] 
    for child in object.children_recursive :
        if child not in childs :
            childs.append(child)  
            
    for ob in objects_list :
        if ob not in childs and len([constraint for constraint in ob.constraints 
                                     if constraint.type == 'CHILD_OF' 
                                     and constraint.target is not None
                                     and constraint.target == object]) > 0 :
            childs.append(ob)
                
    for child in childs :
        if child not in ctrl_list :
            ctrl_list.append(child)
            for granchild in get_constraint_children_recursive(child, objects_list, ctrl_list) :
                if granchild not in childs :
                    childs.append(granchild)
    return childs
def frame_confo(src, tgt, keep=False, verbose = False):
    '''Conform a frame to another without using the blender copy method'''
    #keyframe_type
    tgt.keyframe_type = src.keyframe_type
    i=0
    for src_stroke in src.strokes :
        #search if existing stroke else create new
        if keep and len(tgt.strokes) > i :
            tgt_stroke = tgt.strokes[i]
            i+=1
        else:
            tgt_stroke = tgt.strokes.new()
        stroke_confo(src_stroke, tgt_stroke,keep=keep,verbose=verbose)
    if keep :
        for stroke in tgt.strokes[i:] :
            tgt.strokes.remove(stroke)
    return tgt

def stroke_confo(src, tgt,keep=False, verbose = False,
                  attributes = ['aspect',
                                'display_mode',
                                'end_cap_mode',
                                'hardness',
                                'is_nofill_stroke',
                                'line_width',
                                'material_index',
                                'select',
                                'select_index',
                                'start_cap_mode',
                                'use_cyclic',
                                'uv_rotation',
                                'uv_scale',
                                'uv_translation',
                                'vertex_color_fill',
                                ]):
    '''Conform a stroke to another without using the blender copy method''' 
    for a in attributes :
        match a :
            case _:
                try:
                    setattr(tgt, a, getattr(src, a))
                except:
                    print("Confo strokes point failed on %s, attribute %s"%(tgt,a) )         
    if keep:
        count = len(tgt.points)
        if count < len(src.points):
            tgt.points.add(count = len(src.points)-count)
        elif count > len(src.points):
            for i in range(count-len(src.points)):
                tgt.points.pop()
    else:
        tgt.points.add(count = len(src.points))

    for i,point in enumerate(src.points):        
        point_confo(src.points[i], tgt.points[i], verbose=verbose)
  
def point_confo(src, tgt, attributes=['co',
                                    'pressure',
                                    'select',
                                    'strength',
                                    'uv_factor',
                                    'uv_fill',
                                    'uv_rotation',
                                    'vertex_color']
                ,verbose = False):
    '''Conform a point to another without using the blender copy method'''    
    for a in attributes :
        match a :
            case 'co':
                tgt.co = src.co.copy()
            case _:
                try:
                    setattr(tgt, a, getattr(src, a))
                except:
                    print("Confo strokes point failed on %s, attribute %s"%(tgt,a) )        

def enforce_keyframe(object,i,verbose=False):
    '''enforce a keyframe on an object at a given frame
    args:
        object (object): the object to enforce keyframe on
        i (int): the frame number
        '''
    if object.type == 'GPENCIL' :
        for layer in object.data.layers :
            if get_now_gpframe(i, layer) is None :
                if not layer.active_frame :
                    print('warning : no frame in layer', layer.info , 'of GP ',object.name, '; adding an empty one')
                    new_kf = layer.frames.new(i)
                    new_kf.frame_number = i
                else :
                    new_kf = layer.frames.new(i)
                    frame_confo(layer.active_frame, new_kf)
                    new_kf.frame_number = i
                for stroke in new_kf.strokes :
                    stroke.points.update()
            pass
    if object.animation_data is not None and object.animation_data.action is not None :
        for fcurve in object.animation_data.action.fcurves :
            if get_now_kf(i, fcurve) is None :
                if fc_is_from_array(fcurve):
                    object.keyframe_insert(data_path = fcurve.data_path, frame = i, index = fcurve.array_index)
                else:
                    object.keyframe_insert(data_path = fcurve.data_path, frame = i,index = -1)
    else:
        if verbose:print('warning : no animation data on object', object.name)


def fc_is_from_array(fcurve) :
    if len([fc for fc in fcurve.id_data.fcurves if fc.data_path == fcurve.data_path]) > 1 :
        return True
    else :
        return False
    
def get_fc_array_index(fcurve) :
    '''returns the array index of a fcurve, or -1 if it's not from an array'''
    if not fc_is_from_array(fcurve) :
        fc_index = -1
    else :
        fc_index = fcurve.array_index
    return fc_index

def valid_fcurve(ob, fc) :
    valid = False
    try : 
        ob.path_resolve(fc.data_path)
        valid = True
    except :
        return False
    return valid and fc.is_valid

def valid_driver(ob, driver) :
    valid = False
    try : 
        ob.path_resolve(driver.data_path)
        valid = True
    except : 
        return False
    return valid and driver.is_valid and driver.driver.is_valid
         

def valid_hide_helper(mod, ob, master):
    if any([mod.factor != 0.0,
            mod.hardness != 1.0,
            mod.invert_layer_pass != False,
            mod.invert_layers != False,
            mod.invert_material_pass != False,
            mod.invert_materials != False,
            mod.invert_vertex != False,
            mod.is_override_data != False,
            mod.layer_pass != 0,
            mod.material != None,
            mod.modify_color != 'BOTH',
            mod.pass_index != 9,
            mod.show_expanded != True,
            mod.show_in_editmode != True,
            mod.show_render != True,
            mod.type != 'GP_OPACITY',
            mod.use_custom_curve != False,
            mod.use_normalized_opacity != False,
            mod.use_weight_factor != False]) :

        return False
    has_driver = False
    for driver in ob.animation_data.drivers :
        if driver.data_path =='grease_pencil_modifiers["'+mod.name+'"].show_viewport':
            has_driver = True
            dri = driver.driver
            if any([dri.expression != '1 - CTRL_VISIBILITY',
                    dri.is_simple_expression != True,
                    dri.is_valid != True,
                    dri.type != 'SCRIPTED',
                    dri.use_self != False,
                    len(dri.variables) != 1 ]):
                return False
            var = dri.variables[0]
            if any([var.name != 'CTRL_VISIBILITY',
                    var.is_name_valid != True,
                    len(var.targets) != 1,
                    var.targets[0].id != master,
                    var.type != 'SINGLE_PROP']):

                return False
            break
    
    if not has_driver :

        return False
    return True

def valid_hue_sat(mod, ob):
    if any([
        mod.invert_layer_pass != False,
        mod.invert_layers != False,
        mod.invert_material_pass != False,
        mod.invert_materials != False,
        mod.layer_pass != 0,
        mod.material != None,
        mod.modify_color != 'BOTH',
        mod.pass_index != 0,

        mod.show_expanded != True,
        mod.show_in_editmode != False,
        mod.show_render != False,
        mod.show_viewport != True    
         ]) :
        return False
    return True

def set_hue_sat_mod(mod):
    mod.invert_layer_pass = False
    mod.invert_layers = False
    mod.invert_material_pass = False
    mod.invert_materials = False
    mod.layer_pass = 0
    mod.material = None
    mod.modify_color = 'BOTH'
    mod.pass_index = 0

    mod.show_expanded = True
    mod.show_in_editmode = False
    mod.show_render = False
    mod.show_viewport = True    


def valid_stroke_thick(mod, ob, master):
    if any([
            mod.invert_layer_pass != False,
            mod.invert_layers != False,
            mod.invert_material_pass != True,
            mod.invert_materials != False,
            mod.invert_vertex != False,
            mod.is_override_data != False,
            mod.layer_pass != 0,
            mod.material != None,
            mod.pass_index != 8,
            mod.show_expanded != True,
            mod.show_in_editmode != True,
            mod.show_render != True,
            mod.show_viewport != True,
            mod.thickness != 30,
            #mod.thickness_factor != 1.0,
            mod.type != 'GP_THICK',
            mod.use_custom_curve != False,
            mod.use_normalized_thickness != False,
            mod.use_weight_factor != False]) :
        return False

    has_driver = False
    for driver in ob.animation_data.drivers :
        if driver.data_path =='grease_pencil_modifiers["'+mod.name+'"].thickness_factor':
            has_driver = True
            dri = driver.driver
            if any([dri.expression != 'CTRL_THIK',
                    dri.is_simple_expression != True,
                    dri.is_valid != True,
                    dri.type != 'SCRIPTED',
                    dri.use_self != False,
                    len(dri.variables) != 1 ]):
                return False
            var = dri.variables[0]
            if any([var.name != 'CTRL_THIK',
                    var.is_name_valid != True,
                    len(var.targets) != 1,
                    var.targets[0].id != master,
                    var.type != 'SINGLE_PROP']):
                return False
            break
    if not has_driver : return False
    return True

