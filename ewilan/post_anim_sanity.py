import bpy
from ..element import SANITY_ELEMENT
from .sanity_functions import *



class SC_ANIM_IVANE_MODS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Hue Sat Modifier error (enabled in render) found  : ' 
        self.fixable = True
    def check(self): 
        for ob in [o for o in bpy.data.objects if o.type == 'GPENCIL' and not o.library and 'RIG' in o.name]:
            print(ob.name)
            for mod in [m for m in ob.grease_pencil_modifiers if m.type == 'GP_COLOR']:
                if mod.show_render == True :
                    self.errors.append((ob, mod)) 
    def fix(self):
        for ob, mod in self.errors:
            mod.show_render = False

    def get_report_msg(self, err):
        return self.msg + err[0].name + ' >> ' + err[1].name
    
    def get_fix_msg(self): 
        #customize the fixer button message here (return a string) - delete to use default message
        return 'Fix modifier issue on ' + str(len(self.errors)) + '  objects'      


class SC_ANIM_GP_MASKS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Error found on GPmask : ' 
        self.fixable = True
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.type == 'GPENCIL' :
                if 'gpmdatas' in dir(ob) : #gp masks update
                    for i, gpm in enumerate(ob.gpmdatas) :
                        if gpm.src_ob :
                            if len(gpm.layers) > 0 :
                                if gpm.result_layer_info == '' :
                                    self.errors.append((gpm, ob, 'LAYER_NAME'))
                            else :
                                self.errors.append((gpm, ob, 'NO_LAYERS'))
                        else :
                            self.errors.append((gpm, ob, 'INVALID'))

    def get_gpm_index(self, gpm, gpmdata):
        for i, current_gpm in enumerate(gpmdata) :
            if current_gpm == gpm :
                return i
            
    def fix(self):
        for gpm, ob, errtype in self.errors :
            match errtype :
                case 'INVALID':
                    #try to fix mask

                    for src_ob in bpy.data.objects :
                        if src_ob.library and src_ob.name == ob.name :
                            gpm_index = self.get_gpm_index(gpm, ob.gpmdatas)
                            src_gpm = src_ob.gpmdatas[gpm_index]
                            src_gpm_ob = src_gpm.src_ob
                            gpm.src_ob = bpy.data.objects[src_gpm_ob.name]
                            for layer in gpm.layers :
                                if not layer.ob :
                                    layer.ob = gpm.src_ob    
                            fixed = True
                            break
                        
                case 'NO_LAYERS':    
                    for i, gpmask in enumerate(ob.gpmdatas):
                        if gpmask == gpm :
                            bpy.ops.gpm.remove(gpm=i, object_name=ob.name)

                case 'LAYER_NAME':
                    if len(gpm.result_layer.split('"'))>1 :
                        gpm.result_layer_info = gpm.result_layer.split('"')[1]
        bpy.ops.gpm.switch_all_on()

    def get_fix_msg(self): 
        return 'Fix ' + str(len(self.errors )) + '  Advanced Masks'  
    def get_report_msg(self, err):
        return self.msg + err[1].name +' : '+ err[2]     


class SC_ANIM_BG_TEXTURES(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Invalid BG texture color depth : ' 
        #self.fixable = True
    def check(self): 
        for ob in bpy.context.view_layer.objects :
            if ob.type == 'MESH' and any([tag in ob.name for tag in ['GP_BG', 'GP_PROPS-BG']]) :
                try :
                    png_name = f"{ob.name.replace('PAINT', 'CLEAN')}.png"
                    print(png_name)
                    texture = bpy.data.images[png_name]
                    if ob.library : 
                        lib_path = bpy.path.abspath(ob.library.filepath)
                        texture_path = self.resolve_relative_path(os.path.dirname(lib_path), texture.filepath)
                    else :
                        texture_path = bpy.path.abspath(texture.filepath)

                    if self.get_png_color_depth(texture_path) != 16 :
                        self.errors.append(texture_path)
                except Exception as e : print('err : ', str(e))

    def get_report_msg(self, err):
        return self.msg + err
    def resolve_relative_path(self, base_path, relative_path):
       
        """
        Résout un chemin relatif par rapport à un chemin de base absolu, compatible avec les chemins UNC.

        Args:
            base_path (str): Le chemin absolu du répertoire de base (format UNC supporté).
            relative_path (str): Le chemin relatif à partir du répertoire de base.

        Returns:
            str: Le chemin absolu résultant.
        """
        # Normaliser les chemins pour éviter les problèmes de séparateurs
        base_path = os.path.normpath(base_path)
        relative_path = os.path.normpath(relative_path)
        
        # Si le chemin relatif commence par un chemin absolu, on le retourne directement
        if relative_path.startswith('\\\\srvfiles'):
            return relative_path
        
        # Fractionner les segments relatifs et résoudre manuellement
        base_parts = base_path.split(os.sep)
        relative_parts = relative_path.split(os.sep)

        for part in relative_parts:
            if part == "..":
                # Remonter d'un niveau dans le chemin de base
                if len(base_parts) > 1:  # Éviter de supprimer la racine UNC
                    base_parts.pop()
            elif part and part != ".":
                # Ajouter le segment au chemin de base
                base_parts.append(part)
        # Reconstruire le chemin absolu
        final_path = os.sep.join(base_parts)
        return final_path
    

    def get_png_color_depth(self, image_path):
        """
        Prend le chemin d'une image PNG et retourne la profondeur de couleur en bits par couche.
        
        Args:
            image_path (str): Le chemin vers le fichier PNG.
        
        Returns:
            int: La profondeur de couleur en bits par couche (8 ou 16).
        """
        try:
            with open(image_path, "rb") as f:
                # Vérifier la signature PNG
                signature = f.read(8)
                if signature != b'\x89PNG\r\n\x1a\n':
                    raise ValueError("Ce fichier n'est pas un PNG valide.")
                
                # Lire le chunk IHDR (premier chunk contenant les métadonnées)
                ihdr_length = int.from_bytes(f.read(4), "big")  # Taille du chunk IHDR
                ihdr_type = f.read(4)
                if ihdr_type != b'IHDR':
                    raise ValueError("Le fichier PNG est corrompu ou mal formé (IHDR manquant).")
                
                # Lire les données de l'entête IHDR
                ihdr_data = f.read(ihdr_length)
                bit_depth = ihdr_data[8]  # Le 9e octet (index 8) contient la profondeur en bits par couche
                
                return bit_depth
        except Exception as e:
            print(f"Erreur lors de l'analyse du fichier PNG : {e}")
            return None
