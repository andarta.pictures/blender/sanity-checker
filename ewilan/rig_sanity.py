import bpy
from ..element import SANITY_ELEMENT
from .sanity_functions import *
from mathutils import Matrix, Vector, Euler
import os
  
#----- COLLECTIONS -----------------------------------------------------------------------------------------------------

class SC_COL_RIG(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Cant find the rig collection : should be COL_RIG_' 
        self.parent_checks = ['SC_COL_GP', 'SC_KITSU_CONNECTED', 'SC_CONFIRM_ASSET_NAME']  
    def check(self): 
        col_rig = get_col_rig()
        if not col_rig :
            self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg + get_blender_asset_name()

class SC_COL_ASSETS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Cant find the Asset_emptys_Collection ' 
    def check(self): 
        col_assets = get_asset_empty_col()
        if not col_assets : 
            self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg

class SC_COL_RIG_NAMING(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'wrong collection naming :' 
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
    def check(self): 
        col_rig = get_col_rig()
        for col in col_rig.children_recursive :
            if not col.name.startswith(col_rig.name) :
                self.errors.append(col.name + ' should start with ' + col_rig.name)
    def get_report_msg(self, err):
        return self.msg + err +  'should start with ' + get_col_rig().name

class SC_COL_RIG_COLOR(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.msg = 'wrong collection color (should be GREEN) : ' 
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
    def check(self): 
        col_rig = get_col_rig()
        for col in col_rig.children_recursive :
            if not col.color_tag == 'COLOR_04' :
                self.errors.append(col)
    def get_report_msg(self, err):
        return self.msg + err.name
    def fix(self):
        for col in self.errors :
            col.color_tag = 'COLOR_04'
    def get_fix_msg(self): 
        return 'Change color of ' + str(len(self.errors)) + '  collections'    

#----------- OBJECTS -----------------------------------------------------------------------------------------------------

class SC_FIND_RIG_MASTER(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Cant find Master, ' 
    def check(self): 
        master = get_rig_master()
        if not master :
            self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg + 'should be named CTLR_RIG_' + get_asset_name() + '_MASTER_C and should be inside collection ' + get_col_rig().name

class SC_MASTER_PROPERTIES(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = 'Missing custom property : ' 
    def check(self): 
        master = get_rig_master()
        if 'CTRL_THICKNESS' not in master.keys() :
            self.errors.append('CTRL_THICKNESS')
        if 'CTRL_VISIBILITY' not in master.keys() :
            self.errors.append('CTRL_VISIBILITY')   
    def get_report_msg(self, err):
        return self.msg + err + ' on ' + get_rig_master().name

class SC_FIND_RIG_GLOBAL(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = 'Cant find Global, ' 
    def check(self): 
        col_rig = get_col_rig()
        asset_name = get_asset_name()
        if not 'CTLR_RIG_' + asset_name + '_GLOBAL_C' in col_rig.objects.keys() :
            self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg + 'should be named CTLR_RIG_' + get_asset_name() + '_GLOBAL_C and should be inside collection ' + get_col_rig().name
                
class SC_RIG_OBJECT_NAMING(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG', 'SC_CONFIRM_ASSET_NAME']
        self.msg = 'Bad naming of object : ' 
    def check(self): 
        ob_type_prefixes = ['GP', 'CTLR', 'ARMA', 'REF']
        for ob in get_col_rig().all_objects :
            if len(ob.name) > 4 and ob.name[-4] == '.' :
                self.errors.append(ob.name + ' no .00x allowed')
            if not ( '_' in ob.name and ob.name.split('_')[0] in ob_type_prefixes ):
                self.errors.append(ob.name + ' prefix should be in ' + str(ob_type_prefixes))
            else :
                ob_prefix = ob.name.split('_')[0]
                asset_name = get_blender_asset_name()
                if not ob.name.startswith(ob_prefix +'_RIG_'+ asset_name ) :
                    self.errors.append(ob.name + ' should start with ' + ob_prefix + '_RIG_' + asset_name)

class SC_RIG_OBJECT_PARENTING(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = 'Bad parenting of object : ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if not ob.parent == None :
                self.errors.append(ob.name + 'has direct parentship to ' + ob.parent.name + ', should use Child Of constraint instead')
            has_parent = False
            for c in ob.constraints : ###constraints level :
                if c.type == 'CHILD_OF' :
                    if c.target :
                        has_parent = True
            if not has_parent and ob != get_rig_master() :
                self.errors.append(ob.name + ' is not parented to anything (only the master is allowed)')

class SC_RIG_OBJECT_SETTINGS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Bad setting of object : ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.show_in_front :
                self.errors.append((ob, 'show_in_front', False))
    def fix(self):
        for ob, attr_name, value in self.errors :
            setattr(ob, attr_name, value)
    def get_fix_msg(self): 
        return 'set correctly ' + str(len(self.errors)) + '  object settings'  
    def get_report_msg(self, err):
        return err[0].name + ', attribute : ' + err[1]            
    
class SC_RIG_OBJECT_VERTEX_GROUPS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = 'Useless vertex group found : ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            for vg in ob.vertex_groups : ###vertex groups
                    if not vg.name.startswith('SEL_'):
                        self.errors.append((ob, vg))
    def fix(self):
        for ob, vg in self.errors :
            vg.id_data.vertex_groups.remove(vg)
    def get_fix_msg(self): 
        return 'delete ' + str(len(self.errors)) + '  vertex groups'  
    def get_report_msg(self, err):
        return self.msg + err[1].name +' on '+ err[0].name  +' should start with SEL_ or has to be removed'  
      
class SC_RIG_OBJECT_CONSTRAINT_TARGET(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
    def check(self): 
        for ob in get_col_rig().all_objects :
            for c in ob.constraints :
                if 'target' in dir(c) and c.target == None :
                        self.errors.append('Object ' + ob.name + ' constraint  ' + c.name + ' has no target ')

class SC_RIG_CHILD_OF_INVERSE(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = ' has transforms on inverse matrix on constraint ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            for c in ob.constraints : ###constraints level :
                if c.type == 'CHILD_OF' :
                    if not c.inverse_matrix == Matrix() :
                        self.errors.append((ob, c))                    
    def fix(self):
        for ob, c in self.errors :
            #print(ob.name, '-----', c.name)
            inv_mat = Matrix(c.inverse_matrix)
            i_loc, i_rot, i_sca = Matrix.decompose(inv_mat)
            i_rot = i_rot.to_euler()
            #print(i_loc,i_rot,i_sca)
            if ob.animation_data and ob.animation_data.action :
                for fc in ob.animation_data.action.fcurves : 
                    for kf in fc.keyframe_points :
                        if fc.data_path == 'location':
                            kf.co.y += i_loc[fc.array_index]  
                        if fc.data_path == 'rotation_euler':
                            kf.co.y += i_rot[fc.array_index]
                        if fc.data_path == 'scale':
                            kf.co.y *= i_sca[fc.array_index]
            else :
                ob.location += i_loc
                for i in range(3) :
                    ob.rotation_euler[i] =+ i_rot[i]
                ob.scale *= i_sca
            c.inverse_matrix = Matrix() 
    def get_fix_msg(self): 
        return 'Apply parent inverse matrix' + str(len(self.errors)) + '  child of constraints'  
    def get_report_msg(self, err):
        return err[0].name + self.msg + err[1].name     
 
class SC_RIG_ARMATURE_SETTINGS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = 'Bad setting of Armature (visibility or selectability ) : ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'ARMATURE' :
                if not ob.hide_viewport or not ob.hide_select :
                    self.errors.append(ob)
    def fix(self):
        for ob in self.errors :
            ob.hide_select = True
            ob.hide_viewport = True      
    def get_fix_msg(self): 
        return 'disable visibility/selection of ' + str(len(self.errors)) + '  armatures'  
    def get_report_msg(self, err):
        return self.msg + err.name     

class SC_RIG_ARMATURE_OLD_IK(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = 'Old bug on IK system found : ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'ARMATURE' :
                if 'preholder_3' in ob.pose.bones.keys() :
                    for c in ob.pose.bones['preholder_3'].constraints :
                        if c.type == 'COPY_ROTATION':
                            if c.owner_space != 'CUSTOM' or c.invert_y :
                                if (ob.name, c) not in self.errors :
                                    self.errors.append((ob.name, c)) 
                            if (ob.name.endswith('_R') and ob['FLIP_DRAWINGS'] == False) or (ob.name.endswith('_L') and ob['FLIP_DRAWINGS'] == True) :
                                if (ob.name, c) not in self.errors :
                                    self.errors.append((ob.name, c))
                            for root in get_col_rig().all_objects :
                                for part in root.ik_parts :
                                    if part.armature == c.id_data :
                                        ctlr_etremity = root.ik_parts[2].controller
                                        for co in ctlr_etremity.constraints :
                                            if co.type == 'LIMIT_ROTATION' :
                                                if (ob.name, c) not in self.errors :
                                                    self.errors.append((ob.name, c))                                             
                                        break
    def fix(self):
        for obname, c in self.errors :
            c.owner_space = 'CUSTOM'
            c.invert_y = False
            arma = c.id_data 
            master = get_rig_master()
            c.space_object = master    
            if arma.name.endswith('_R') :
                arma['FLIP_DRAWINGS'] = True
            else :
                arma['FLIP_DRAWINGS'] = False
            for root in bpy.context.scene.objects :
                for part in root.ik_parts :
                    if part.armature == arma :
                        ctlr_etremity = root.ik_parts[2].controller
                        evil_co = None
                        for co in ctlr_etremity.constraints :
                            if co.type == 'LIMIT_ROTATION' :
                                evil_co = co                               
                            if co.type == 'CHILD_OF' and arma.name.endswith('_R') :
                                co.target = bpy.data.objects[master.name + '_FLIP']
                        if evil_co :
                            ctlr_etremity.constraints.remove(co)
                        break 
    def get_fix_msg(self): 
        return 'fix ' + str(len(self.errors)) + ' specific ik bug from old system'  
    def get_report_msg(self, err):
        return self.msg + err[0]   

#---------GREASE PENCIL -----------------------------------------------------------------------------------------------------    

class SC_RIG_GP_SETTINGS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Bad setting of grease pencil : ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                gp = ob.data
                if gp.stroke_depth_order != '2D' :
                    self.errors.append((gp, 'stroke_depth_order', '2D'))
                if gp.stroke_thickness_space != 'WORLDSPACE' :
                    self.errors.append((gp, 'stroke_thickness_space', 'WORLDSPACE'))   
    def fix(self):
        for gp, attr_name, value in self.errors :
            setattr(gp, attr_name, value)
    def get_fix_msg(self): 
        return 'set correctly ' + str(len(self.errors)) + '  grease pencil settings'  
    def get_report_msg(self, err):
        return self.msg + err[0].name + ', attribute : ' + err[1]        

class SC_RIG_GP_NAME(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Bad naming of grease pencil : ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                gp = ob.data
                if gp.users == 1 :
                    if not gp.name == ob.name.replace(ob.name.split('_')[0], 'GPSH') :
                        correct_name = ob.name.replace(ob.name.split('_')[0], 'GPSH')
                        self.errors.append((gp, correct_name))               
    def fix(self):
        for gp, name in self.errors :
            gp.name = name
    def get_fix_msg(self): 
        return 'Rename ' + str(len(self.errors)) + '  grease pencils'  
    def get_report_msg(self, err):
        return self.msg + err[0].name + ', should be : ' + err[1]     
       
class SC_RIG_GPLAYER(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'GPLayer error : ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                gp = ob.data
                for layer in gp.layers :  
                    if  layer.matrix_layer != Matrix() and ob.name.split('_')[0] != 'CTLR' and 'BCK' not in layer.info and 'HELPERS' not in layer.info and 'mask' not in layer.info and 'IRIS' not in layer.info:
                        self.errors.append('Object ' + ob.name + ' GP : ' + gp.name + ' Layer ' + layer.info + ' has transforms on its layer_matrix (allowed only for CTLR objects, HELPERS layers, BCK layers, mask layers, IRIS layers')
                    if not layer.parent == None :
                        self.errors.append('Object ' + ob.name + ' GP : ' + gp.name + ' Layer ' + layer.info + 'is parented to ' + layer.parent.name + ' => no layer parenting')
                    if not len(layer.frames) > 0 :
                        self.errors.append('Empty layer on ' + ob.name + ' GP : ' + gp.name + ' Layer ' + layer.info)

class SC_RIG_GPLAYER_PREFIX(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Bad layer naming (should start with "GPLYR_") : ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                gp = ob.data
                for layer in gp.layers :  
                    if not (layer.info.startswith('GPLYR_') or layer.info == 'TEMP_GUIDE') :
                        self.errors.append((ob, layer))
    def fix(self):
        for ob, layer in self.errors :
            layer.info = 'GPLYR_'+layer.info
    def get_fix_msg(self): 
        return 'Prefix ' + str(len(self.errors)) + ' layer names'
    def get_report_msg(self, err):
        return self.msg + 'Object : ' + err[0].name + ' GP : ' + err[0].data.name + ' Layer : '+ err[1].info  
    
class SC_RIG_GPLAYER_USE_LIGHT(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.msg = 'Use lights enabled on GP layer : ' 
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.type == 'GPENCIL' and not ob.library :
                gp = ob.data
                for layer in gp.layers :  
                    if layer.use_lights :
                        self.errors.append((ob, layer))
    def fix(self):
        for ob, layer in self.errors :
            layer.use_lights = False
    def get_fix_msg(self): 
        return 'Disable use light of ' + str(len(self.errors)) + ' layers'
    def get_report_msg(self, err):
        return self.msg + 'Object : ' + err[0].name + ' GP : ' + err[0].data.name + ' Layer : '+ err[1].info  
    
class SC_RIG_GPLAYER_ROUGH(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'No rough layer found on object ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' and not ob.library :
                gp = ob.data
                found = False
                for layer in gp.layers :  
                    if layer.info == 'GPLYR_RGH' :
                        found = True
                if not found and 'FLIP' not in ob.name and 'HLP' not in ob.name and 'CTLR' not in ob.name  :
                    self.errors.append(ob)                
    def fix(self):
        for ob in self.errors :
            rgh_layer = ob.data.layers.new('GPLYR_RGH')
            rgh_layer.use_lights = False
            new_frame = rgh_layer.frames.new(0)
            new_frame.keyframe_type = 'JITTER'
    def get_fix_msg(self): 
        return 'Add rough layer on ' + str(len(self.errors)) + ' GP objects'
    def get_report_msg(self, err):
        return self.msg + err.name
    
class SC_RIG_GPLAYER_ROUGH_EMPTY(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()  
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']      
        self.msg = 'Drawings found on Rough layer : ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                ob_errs = []
                for layer in ob.data.layers :
                    if layer.info == 'GPLYR_RGH':
                        for frame in layer.frames :
                            if len(frame.strokes) != 0 :
                                ob_errs.append(frame.frame_number)
                if len(ob_errs) > 0 :
                    self.errors.append((ob.name, ob_errs))

    def get_report_msg(self, err):
        return self.msg + err[0] + ':' + str(err[1])
    
class SC_RIG_ACTION_NAME(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Bad naming of action on object: ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' and not ob.library :
                if ob.animation_data and ob.animation_data.action :
                    if ob.animation_data.action.name != ob.name + '_ACT' :
                        self.errors.append((ob, ob.animation_data.action))              
    def fix(self):
        for ob, action in self.errors :
            action.name = ob.name + '_ACT'
    def get_fix_msg(self): 
        return 'Rename ' + str(len(self.errors)) + ' actions'
    def get_report_msg(self, err):
        return self.msg + err[0].name

class SC_RIG_GP_MATERIALS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Material : ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                for mat in ob.data.materials : ### gp materials level
                    if mat is not None :
                        if mat.library == None and mat.name not in ['GUIDE_MAT', 'GPMAT_mask_material'] :
                            self.errors.append(mat.name + ' is not a linked material.')
                        if mat.name == 'GUIDE_MAT' :
                                found_guide_mat = True
                        if not (mat.name.startswith('GPMAT_') or mat.name == 'GUIDE_MAT'): 
                            self.errors.append(mat.name + ' name should start with "GPMAT_"')


class SC_RIG_LATERALIZED_OBJS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        #self.fixable = True
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Brokent pair of lateralized objets : Cant find ' 
    def check(self): 
        col_rig = get_col_rig()
        ob_names = [ob.name for ob in col_rig.all_objects]
        for ob in col_rig.all_objects :
            if ob.type == 'GPENCIL' and not ob.library :
                pair_name = None
                if ob.name[-2:] == '_L':
                    pair_name = ob.name[:-2] + '_R'
                if ob.name[-2:] == '_R':
                    pair_name = ob.name[:-2] + '_L'
                if pair_name and pair_name not in ob_names :
                    self.errors.append(pair_name)

    def get_report_msg(self, err):
        return self.msg + err

class SC_RIG_LAT_MATERIAL_LIST(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG', 'SC_RIG_LATERALIZED_OBJS']
        self.msg = 'Material lists of lateralized objets do not match : ' 
    def check(self): 
        col_rig = get_col_rig()
        for ob in col_rig.all_objects :
            if ob.type == 'GPENCIL' and not ob.library :
                ob_errs = []
                gp = ob.data
                if ob.name[-2:] == '_L':
                    pair_name = ob.name[:-2] + '_R'
                    ob2 = col_rig.all_objects[pair_name]
                    if len(ob.material_slots) != len(ob2.material_slots):
                        ob_errs.append('SLOTS NUMBER')
                    elif 'CTLR' not in ob.name :
                        for i, slot in enumerate(ob.material_slots) :
                            if slot.material != ob2.material_slots[i].material :
                                ob_errs.append('SLOT No ' + str(i))
                if len(ob_errs) > 0 :
                    self.errors.append((ob.name, ob_errs))

    def get_report_msg(self, err):
        return self.msg + err[0] + str(err[1])

class SC_RIG_GP_MASKS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Error found on GPmask : ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                if 'gpmdatas' in dir(ob) : #gp masks update
                    for gpm in ob.gpmdatas :
                        if gpm.src_ob and len(gpm.layers) > 0 :
                            if gpm.result_layer_info == '' :
                                self.errors.append((gpm, ob, 'LAYER_NAME'))
                        else :
                            self.errors.append((gpm, ob, 'INVALID'))
    def fix(self):
        for gpm, ob, errtype in self.errors :
            match errtype :
                case 'INVALID':
                    for i, gpmask in enumerate(ob.gpmdatas):
                        if gpmask == gpm :
                            bpy.ops.gpm.remove(gpm=i, object_name=ob.name)
                case 'LAYER_NAME':
                    if len(gpm.result_layer.split('"'))>1 :
                        gpm.result_layer_info = gpm.result_layer.split('"')[1]
    def get_fix_msg(self): 
        return 'Fix ' + str(len([err for err in self.errors if err[2] == 'LAYER_NAME'])) + ' and delete ' + str(len([err for err in self.errors if err[2] == 'INVALID'])) + '  Advanced Masks'  
    def get_report_msg(self, err):
        return self.msg + err[1].name      

class SC_RIG_GP_MODIFIERS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_FIND_RIG_MASTER']
        self.msg = 'Error found on modifer : ' 
        self.fixable = True
    def check(self): 
        master = get_rig_master()
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' :
                gp = ob.data
                found_hide_hlp = False
                found_stroke_thick = False
                for mod in ob.grease_pencil_modifiers :###GP modifiers level
                    if mod.name.endswith('HUE_SAT') :
                        if not valid_hue_sat(mod,ob):
                            self.errors.append((mod, ob, 'HS', 'INVALID'))
                    if mod.name.endswith('HIDE_HELPERS') :
                        found_hide_hlp = True 
                        if mod.name != ob.name + '_HIDE_HELPERS' :
                            self.errors.append((mod, ob, 'HH', 'BADNAME'))
                        if not valid_hide_helper(mod, ob, master):
                            self.errors.append((mod, ob, 'HH', 'INVALID'))                                                           
                    if mod.name.endswith('STROKE_THICKNESS') :
                        found_stroke_thick = True
                        if mod.name != ob.name + '_STROKE_THICKNESS' :
                            self.errors.append((mod, ob, 'ST', 'BADNAME'))
                        if not valid_stroke_thick(mod, ob, master):
                            self.errors.append((mod, ob, 'ST', 'INVALID'))
                if not found_hide_hlp : 
                    self.errors.append((None, ob, 'HH', 'MISSING'))  
                if not found_stroke_thick :        
                    self.errors.append((None, ob, 'ST', 'MISSING'))                
    def fix(self):
        if 'rigtools' in dir(bpy.ops):
            master = get_rig_master()
            mods_to_del = {}
            for mod, ob, type, err in self.errors :
                if mod and type != 'HS':
                    for dri in ob.animation_data.drivers :
                        if type == 'HH' and dri.data_path == 'grease_pencil_modifiers["'+mod.name+'"].show_viewport' or type == 'ST' and dri.data_path == 'grease_pencil_modifiers["'+mod.name+'"].thickness_factor':
                            ob.animation_data.drivers.remove(dri)
                    mods_to_del[mod] = ob

            for mod in mods_to_del :
                ob = mods_to_del[mod]
                ob.grease_pencil_modifiers.remove(mod)
            
            for mod, ob, type, err in self.errors :
                match type :    
                    case 'HH':
                        bpy.ops.rigtools.add_hide_helpers_mod(object_name = ob.name, master_name = master.name)
                    case 'ST':
                        bpy.ops.rigtools.add_stroke_thick_mod(object_name = ob.name, master_name = master.name)  
                    case 'HS':
                        set_hue_sat_mod(mod)
        else :
            print("can't clean modifiers : you need to enable andarta rig toolbox add-on")
    def get_fix_msg(self): 
        return 'Fix ' + str(len(self.errors)) + ' grease pencil modifiers'  
    def get_report_msg(self, err):
        return self.msg + err[1].name + ', ' + err[2] + ':' + err[3]        

class SC_RIG_TEMP_GUIDE_LAYER(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()  
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']      
        self.msg = 'TEMP_GUIDE layers has to be removed from object '     
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL':
                for layer in ob.data.layers :
                    if layer.info == 'TEMP_GUIDE':
                        self.errors.append(ob)
                        break  
    def get_report_msg(self, err):
        return self.msg + err.name

class SC_RIG_GUIDE_MATERIAL(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()  
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']      
        self.msg = 'GUIDE_MAT material has to be removed from materials of object '     
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL':
                for mat in ob.data.materials : ### gp materials level
                    if mat and mat.name == 'GUIDE_MAT' :
                        self.errors.append(ob)
                        break  
    def get_report_msg(self, err):
        return self.msg + err.name

class SC_RIG_UNCLOSED_GP_STROKE(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_GPENCIL_NO_MATERIAL_STROKE']
        self.msg = 'Found non closed Fill strokes : ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' and not ob.library :
                gp = ob.data
                strokes = []
                for layer in gp.layers :  
                    for frame in layer.frames :
                        for stroke in frame.strokes :
                            stroke_mat = ob.material_slots[stroke.material_index].material
                            if stroke_mat and stroke_mat.grease_pencil.show_fill and not stroke.use_cyclic :
                                strokes.append(stroke)
                if len(strokes) > 0 :
                    self.errors.append((ob,strokes))
    def fix(self):
        for ob, strokes in self.errors :
            for stroke in strokes :
                stroke.use_cyclic = True
    def get_fix_msg(self): 
        return 'Close fill strokes on' + str(len(self.errors)) + ' objects '
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' : ' + str(len(err[1])) + ' strokes'


class SC_RIG_PALETTE_NAMING(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_CONFIRM_ASSET_NAME']
        self.msg = 'Bad naming of Palette : ' 
    def check(self): 
        asset_name = get_blender_asset_name()
        for pal in bpy.context.scene.gpmatpalettes.palettes :
            if pal.name not in ['PLT_EWI_RIG']:
                if not pal.name.startswith("PLT_") or not asset_name == pal.name.split('_')[2]:
                    self.errors.append((pal.name, asset_name))

    def get_report_msg(self, err):
        return self.msg + err[0] + f" should start with PLT_CH_{err[1]}"


#------- ANIM DATA -----------------------------------------------------------------------------------------------------
    

class SC_GPENCIL_ORIENTATION(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()     
        self.msg = 'Bad orientation of object : ' 
        self.fixable = True
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.type == 'GPENCIL':
                if ob.animation_data and ob.animation_data.action :    
                    kfs = []                
                    for fcurve in ob.animation_data.action.fcurves : 
                        if fcurve.data_path == 'rotation_euler' and fcurve.array_index in [0,2]:
                            for kf in fcurve.keyframe_points :
                                if kf.co.y != 0.0 :
                                    kfs.append(kf)
                    if len(kfs) > 0 :
                        self.errors.append((ob, kfs))
                else : 
                    if not (ob.rotation_euler[0] == 0 and ob.rotation_euler[2] == 0) :
                        self.errors.append((ob, None))
    def fix(self):
        for ob, kfs in self.errors :
            if kfs :
                for kf in kfs :
                    kf.co.y = 0
            else :
                for i in [0,2]:
                    ob.rotation_euler[i] = 0                 
    def get_fix_msg(self): 
        return 'Zero X Z rotation on ' + str(len(self.errors)) + '  objects'  
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' on frames ' + str([kf.co.x for kf in err[1]]) 
    
class SC_RIG_GPENCIL_CONSTANT_KF(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()     
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']   
        self.msg = 'found non CONSTANT keyframes on ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL':
                kfs = []
                if ob.animation_data and ob.animation_data.action :                    
                    for fcurve in ob.animation_data.action.fcurves : 
                        for kf in fcurve.keyframe_points :
                            if kf.interpolation != 'CONSTANT':
                                kfs.append(kf)
                if len(kfs) > 0 :
                    self.errors.append((ob, kfs))
    def fix(self):
        for ob, kfs in self.errors :
            for kf in kfs :
                kf.interpolation = 'CONSTANT'
    def get_fix_msg(self): 
        return 'force constant interpolation mode on ' + str(len(self.errors)) + '  objects'  
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' on frames ' + str([int(kf.co.x) for kf in err[1]])    

class SC_RIG_GPENCIL_MAX_Y(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()  
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']   
        self.msg = 'Excessive value on Y axis location. Object :  ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL':
                kfs = []
                if ob.animation_data and ob.animation_data.action :                    
                    for fcurve in ob.animation_data.action.fcurves : 
                        if fcurve.data_path == 'location' and fcurve.array_index == 1 :
                            for kf in fcurve.keyframe_points :
                                if not 0.1 > kf.co.y > -0.1 :
                                    kfs.append(kf)
                if len(kfs) > 0 :
                    self.errors.append((ob, kfs))
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' on frames ' + str([int(kf.co.x) for kf in err[1]])     

class SC_RIG_DELTA_TRANSFORMS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()  
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']   
        self.msg = 'Delta transforms found on object :  ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if any([ob.delta_location[0] != 0.0, 
                    ob.delta_location[2] != 0.0,
                    ob.delta_rotation_euler != Euler(), 
                    abs(ob.delta_scale[0]) != 1,
                    ob.delta_scale[1] != 1,
                    ob.delta_scale[2] != 1
                    ]):  
            
                self.errors.append((ob))
    def get_report_msg(self, err):
        return self.msg + err.name     

class SC_RIG_GPENCIL_SCALE(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()  
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']   
        self.msg = 'Scale found on object :  ' 
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.type == 'GPENCIL' and not 'EYE_HLP' in ob.name :
                kfs = []
                if ob.animation_data and ob.animation_data.action :                    
                    for fcurve in ob.animation_data.action.fcurves : 
                        if fcurve.data_path == 'scale':
                            for kf in fcurve.keyframe_points :
                                if round(kf.co.y, 5) != 1.0 :
                                    if int(kf.co.x) not in kfs :
                                        kfs.append(int(kf.co.x))
                if len(kfs) > 0 :
                    self.errors.append((ob, kfs))
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' on frames ' + str([kf for kf in err[1]])     

class SC_GPENCIL_Y_ON_POINTS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()     
        self.msg = 'Y transforms on points :  Object: ' 
        self.fixable = True
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.type == 'GPENCIL' and not (ob.library or ob.data.library) and ob.name[:4] != 'CTLR':
                frames = []
                for layer in ob.data.layers :
                    for frame in layer.frames :
                        for stroke in frame.strokes :
                            for point in stroke.points : 
                                if point.co[1] != 0.0  :
                                    frames.append(frame)
                                    break
                if len(frames) > 0 :
                    self.errors.append((ob, frames))   
    def fix(self):
        for ob, frames in self.errors :
            for frame in frames :
                for stroke in frame.strokes :
                    for point in stroke.points :
                        if point.co[1] != 0.0 :
                            point.co[1] = 0.0 

    def get_fix_msg(self): 
        return 'zero Y location on points on ' + str(len(self.errors)) + '  objects'  
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' on frames ' + str([frame.frame_number for frame in err[1]])    

class SC_RIG_OBJECT_NO_ANIMDATA(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()  
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG']      
        self.msg = 'No action or animation data on Object: ' 
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if not ('FLIP' in ob.name or 'ARMA' in ob.name) :
                if ob.animation_data :
                    if ob.animation_data.action == None :
                        self.errors.append(ob)
                else :
                    self.errors.append(ob)
    def fix(self):
        for ob in self.errors :
            ob.keyframe_insert('location', index= -1, frame=0)
            ob.keyframe_insert('rotation_euler', index= -1, frame=0)
            ob.keyframe_insert('scale', index= -1, frame=0)
    def get_fix_msg(self): 
        return 'Create action / anim data for ' + str(len(self.errors)) + '  objects'  
    def get_report_msg(self, err):
        return self.msg + err.name + ' this is allowed for FLIP and ARMA only'
    

    
class SC_RIG_ZERO_KEYFRAME(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_GP', 'SC_COL_RIG','SC_VALID_FCURVES']
        self.msg = 'Need to enforce / clean the 0 keyframe'
        self.fixable = True
    def check(self): 
        for ob in get_col_rig().all_objects :
            if ob.animation_data and ob.animation_data.action :
                for fc in ob.animation_data.action.fcurves :
                    now_kf =  get_now_kf(0, fc) 
                    if now_kf :
                        if now_kf.type != 'JITTER':
                            self.errors.append(True)
                            return
                    else : 
                        self.errors.append(True)
                        return    
            if ob.type == 'GPENCIL' :
                gp = ob.data
                for layer in gp.layers : ### layer level
                    zero_frame = get_now_gpframe(0, layer)
                    if zero_frame :
                        if zero_frame.keyframe_type != 'JITTER' :
                            self.errors.append(True)
                            return
                    else :
                        self.errors.append(True)
                        return
                if gp.animation_data and gp.animation_data.action :
                    for fc in gp.animation_data.action.fcurves :
                        now_kf =  get_now_kf(0, fc) 
                        if now_kf :
                            if now_kf.type != 'JITTER':
                                self.errors.append(True)
                                return
                        else : 
                            self.errors.append(True)
                            return   
    def fix(self):
        init_frame = bpy.context.scene.frame_current
        bpy.context.scene.frame_set(0)
        for ob in get_col_rig().all_objects :
            if ob.animation_data and ob.animation_data.action :
                ob.matrix_local = Matrix()
                for fc in ob.animation_data.action.fcurves : 
                    ob.keyframe_insert(data_path = fc.data_path, index = get_fc_array_index(fc),frame=0)
                    kf = get_now_kf(0, fc)
                    kf.type = 'JITTER'  
                    match fc.data_path :
                        case 'location' :
                            kf.co.y = 0
                        case 'rotation_euler' :
                            kf.co.y = 0
                        case 'scale' :
                            kf.co.y = 1  
            if ob.type == 'GPENCIL' :
                for layer in ob.data.layers :
                    frame = get_now_gpframe(0, layer)
                    if not frame :
                        frame = layer.frames.new(0)
                    frame.keyframe_type = 'JITTER'
                if ob.data.animation_data and ob.data.animation_data.action :
                    for fc in ob.data.animation_data.action.fcurves : 
                        ob.data.keyframe_insert(data_path = fc.data_path, index = get_fc_array_index(fc),frame=0)
                        kf = get_now_kf(0, fc)
                        if kf :
                            kf.type = 'JITTER' 
                        #else : print('!!!',ob.data.name , fc.data_path)   
                        
        bpy.context.scene.frame_set(init_frame)
    def get_fix_msg(self): 
        return 'Enforce the zero keyframe'  
    def get_report_msg(self, err):
        return self.msg
    
#-------ASSETS -----------------------------------------------------------------------------------------------------
class SC_RIG_ASSET_ROOT(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_ASSETS','SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'No root object for Asset : '
    def check(self): 
        asset_col = get_asset_empty_col()
        for aob in asset_col.all_objects :
            if aob.animation_data and aob.animation_data.action is not None and aob.animation_data.action.asset_data is not None :
                action = aob.animation_data.action
                asset_name = action.name
                import_data = action.import_data
                root_names = import_data.get_selected_roots_names()
                if len(root_names) == 0 :
                    self.errors.append(action.name + ' has no root object')
                for root_name in root_names :
                    if root_name not in bpy.context.scene.objects.keys() :
                        self.errors.append(action.name + ' has an invalid root object')

                 

class SC_RIG_ASSET_GIF(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_COL_ASSETS','SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'No .GIF file found for Asset :'
    def check(self): 
        asset_col = get_asset_empty_col()
        for aob in asset_col.all_objects :
            if aob.animation_data and aob.animation_data.action is not None and aob.animation_data.action.asset_data is not None :
                action = aob.animation_data.action
                asset_name = action.name
                gif_path = os.path.join(os.path.dirname(bpy.data.filepath), asset_name, '.GIF')
                if not os.path.exists(gif_path) :
                    self.errors.append(action.name)

class SC_RIG_CH_ASSET_CATALOG(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.parent_checks = ['SC_COL_ASSETS','SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Invalid catalog UUID for Asset :'
    def check(self): 
        asset_col = get_asset_empty_col()
        for aob in asset_col.all_objects :
            if aob.animation_data and aob.animation_data.action is not None and aob.animation_data.action.asset_data is not None :
                action = aob.animation_data.action
                asset_data = action.asset_data
                if asset_data.catalog_id not in ['00000001-0001-0000-0000-000000000000',
                                                '00000001-0002-0000-0000-000000000000',
                                                '00000001-0003-0000-0000-000000000000',
                                                '00000001-0004-0000-0000-000000000000']  :
                    self.errors.append(action)
    def fix(self):
        for action in self.errors :
            asset_data = action.asset_data
            asset_name = action.name
            if 'TURN' in asset_name or 'ORIENTATION' in asset_name :
                asset_data.catalog_id = '00000001-0001-0000-0000-000000000000'
            elif 'LIPS' in asset_name :
                asset_data.catalog_id = '00000001-0002-0000-0000-000000000000'
            elif 'EXPR' in asset_name :
                asset_data.catalog_id = '00000001-0003-0000-0000-000000000000'
            else : 
                asset_data.catalog_id = '00000001-0004-0000-0000-000000000000'                           
    def get_fix_msg(self): 
        return 'Fix catalog UUID for ' + str(len(self.errors)) + ' assets.' 
    def get_report_msg(self, err):
        return self.msg + err.name

class SC_RIG_ASSET_MC_KF_ENFORCEMENT(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.parent_checks = ['SC_COL_ASSETS','SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'No enforced keyframe found for MC Asset :'
    def check(self): 
        asset_col = get_asset_empty_col()
        for aob in asset_col.all_objects :
            if aob.animation_data and aob.animation_data.action is not None and aob.animation_data.action.asset_data is not None :
                action = aob.animation_data.action
                import_data = action.import_data
                if import_data.asset_type == 'MC': #Mastercontroller found
                                if not has_enforced_kf(import_data, get_col_rig()) :
                                    self.errors.append(action)
    def fix(self):
        rig_col = get_col_rig()
        for action in self.errors :
            import_data = action.import_data
            root_objects = [r.object for r in import_data.id_data.asset_root_objects]
            for ob in get_objects_and_childs(root_objects, rig_col.all_objects):
                for t in range(import_data.range[0], import_data.range[1]+1) :
                    bpy.context.scene.frame_set(t)
                    bpy.context.view_layer.update()
                    enforce_keyframe(ob, t)
                update_points_on_gp(ob)
                force_constant_kf(ob)
    def get_fix_msg(self): 
        return 'Enforce keyframes for ' + str(len(self.errors)) + ' MC assets.' 
    def get_report_msg(self, err):
        return self.msg + err.name
    
class SC_RIG_ASSET_INVALID_EMPTY(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.fixable = True
        self.parent_checks = ['SC_COL_ASSETS','SC_COL_GP', 'SC_COL_RIG']
        self.msg = 'Invalid Asset empty :'
    def check(self): 
        asset_col = get_asset_empty_col()
        for aob in asset_col.all_objects :
            if not (aob.animation_data and aob.animation_data.action is not None and aob.animation_data.action.asset_data is not None ):
                self.errors.append(aob)
    def fix(self):
        for aob in self.errors :
            bpy.data.objects.remove(aob)
    def get_fix_msg(self): 
        return 'Delete ' + str(len(self.errors)) + ' invalid asset emptys.' 
    def get_report_msg(self, err):
        return self.msg + err.name