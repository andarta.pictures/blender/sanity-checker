import bpy
from ..element import SANITY_ELEMENT
from .sanity_functions import *

class SC_EXAMPLE(SANITY_ELEMENT):
    #Template for sanity check element (checkpoint)
    def __init__(self):
        super().__init__()
        self.msg = 'BAD NAMING (A instead of Z)' #default sanity report message is this string followed by str(error) for error in self.errors
        self.fixable = True #set this to True if there is a fix() method to fix errors       
        self.parent_checks = ['SC_COL_GP']  # list here parent checkpoint class name that have to return no errors for running this checkpoint 
    def check(self): 
        #check for errors, if found, append anything you will need to fix to self.errors (at least None)
        for ob in bpy.context.scene.objects:
            if 'A' in ob.name :
                self.errors.append((ob, ob.name.replace('A','Z')))
    def fix(self): 
        #from what is stored in self.errors, fix the error in this method
        for ob, name in self.errors :
            ob.name = name    
    def get_fix_msg(self): 
        #customize the fixer button message here (return a string) - delete to use default message
        return 'Rename ' + str(len(self.errors)) + '  objects'    
    def get_report_msg(self, err):
        #customize the sanity report messages here (return a list of strings) - delete to use default message
        return self.msg + ' : ' + str(err)
    
class SC_FIND_GPMAT_OBJECT(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'No GP_MAT object found' 
    def check(self): 
        if not 'GP_MAT' in bpy.data.objects :
            self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg 
    
class SC_GPMAT_EMPTY(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Empty material slot found in GP_MAT.' 
        self.parent_checks = ['SC_FIND_GPMAT_OBJECT']  
    def check(self): 
        for mat in bpy.data.objects['GP_MAT'].data.materials :
            if mat is None : 
                self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg
    
class SC_GPMAT_PREFIX(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'no GPMAT_ prefix on material ' 
        self.parent_checks = ['SC_GPMAT_EMPTY']  
    def check(self): 
        for mat in bpy.data.objects['GP_MAT'].data.materials :
            if not mat.name.startswith('GPMAT_') :
                self.errors.append(mat)
    def get_report_msg(self, err):
        return self.msg + err.name
    
class SC_GPMAT_NAMING(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Incorrect asset naming, should be :  ' 
        self.parent_checks = ['SC_KITSU_CONNECTED', 'SC_CONFIRM_ASSET_NAME', 'SC_FIND_GPMAT_OBJECT']  
    def check(self): 
        for mat in bpy.data.objects['GP_MAT'].data.materials :
            if not  get_blender_asset_name() in  mat.name :
                self.errors.append(mat)
    def get_report_msg(self, err):
        return self.msg + get_blender_asset_name() + ' found : ' + err.name
    
mat_type_suffixes = ['FILL', 'STROKE', 'TEX']

class SC_GPMAT_SETTINGS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'no valid suffix on material ' 
        self.parent_checks = ['SC_FIND_GPMAT_OBJECT', "SC_GPMAT_EMPTY"]  
    def check(self): 
        for mat in bpy.data.objects['GP_MAT'].data.materials :
            suffixed = False
            for suffix in mat_type_suffixes :
                if mat.name.endswith(suffix) :
                    suffixed = True
            if not suffixed :    
                self.errors.append('no valid suffix on material ' + mat.name + '. available suffixes : ' + str(mat_type_suffixes))
            else :
                if mat.grease_pencil.show_fill and mat.grease_pencil.show_stroke and not mat.name.endswith('TEX') :
                    self.errors.append('material '+ mat.name + ' is both stroke and fill (for SHADOWS_TEX materials only)')
                elif mat.grease_pencil.show_stroke : 
                    if mat.name.endswith('TEX') :
                        if not mat.grease_pencil.mode == 'DOTS':
                            self.errors.append('Bad Line type on texture material ' + mat.name + ' : should be Dots')
                        if not mat.grease_pencil.stroke_style == 'TEXTURE':
                            self.errors.append('Bad stroke style on texture material ' + mat.name + ' : should be Texture')
                        if not mat.grease_pencil.pass_index == 8 :
                            self.errors.append('Bad pass index on texture material ' + mat.name + ' : should be 8')
                    elif mat.name.endswith('STROKE') :
                        if not mat.grease_pencil.mode == 'LINE':
                            self.errors.append('Bad Line type on texture material ' + mat.name + ' : should be LINE')
                    elif mat.name.endswith('FILL') :
                        self.errors.append('Material ' + mat.name + ' is suffixed FILL but looks more like a stroke or texture material')
                    if mat.grease_pencil.use_stroke_holdout and not mat.name.endswith('TEX') :
                        self.errors.append('material ' + mat.name + ' is set as a holdout material')
                elif mat.grease_pencil.show_fill : 
                    if not mat.name.endswith('FILL') :
                        self.errors.append('Material'+ mat.name +'is a fill material but has another suffix')
                    if mat.grease_pencil.use_fill_holdout :
                        self.errors.append('material ' + mat.name + ' is set as a holdout material')
    def get_report_msg(self, err):
        return err
    
