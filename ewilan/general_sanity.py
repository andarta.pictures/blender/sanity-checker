import bpy
from ..element import SANITY_ELEMENT
from .sanity_functions import * 
from ..common.import_utils import ensure_lib
mandatory_libs = [
    "gazu",
    ]
for lib in mandatory_libs :
    has_lib = ensure_lib(lib)
    if not has_lib :
        raise Exception('Error: Could not install "' + lib + '\n Try again and if this error persists install dependencies manually')
import gazu
import os
    
class SC_COL_GP(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Dont find COL-GP Collection ' 
    def check(self): 
        col_gp = get_col_gp()
        if not col_gp : 
            self.errors.append(None)
    def get_report_msg(self, err):
        return self.msg
    
class SC_SCENE_UNIT_SCALE(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Bad unit scale in this scene (should be 0.1)'
        self.fixable = True
    def check(self): 
        if round(bpy.context.scene.unit_settings.scale_length, 5) != 0.1:
            self.errors.append('Found' + str(bpy.context.scene.unit_settings.scale_length))
    def fix(self):
        bpy.context.scene.unit_settings.scale_length = 0.1
    def get_fix_msg(self): 
        return 'Change scene unit scale to 0.1'
    
class SC_GPENCIL_2_POINTS_STROKE(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()     
        self.msg = '1 or 2 point strokes found :  Object: ' 
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.type == 'GPENCIL' and not (ob.library or ob.data.library) and ob.name[:4] != 'CTLR':
                layers = []
                for layer in ob.data.layers :
                    frames = []
                    for frame in layer.frames :
                        for stroke in frame.strokes :
                            if len(stroke.points) <= 2 :
                                frames.append(frame.frame_number)
                    if len(frames) > 0 :
                        layers.append((layer.info, frames))   
                if len(layers) > 0 :
                    self.errors.append((ob, layers))
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' => ' + str(err[1])  
    
class SC_VALID_DRIVERS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()     
        self.msg = 'Invalid driver found : ' 
        self.fixable = True
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.animation_data : ### anim data level  
                ob.animation_data.drivers.update()         
                for driver in ob.animation_data.drivers :
                    if not valid_driver(ob, driver) :
                        self.errors.append((ob, ob.animation_data.drivers,driver))
            if ob.type == 'GPENCIL' and ob.data and ob.data.animation_data :                       
                ob.data.animation_data.drivers.update()
                for driver in ob.data.animation_data.drivers :
                    if not valid_driver(ob.data, driver) :
                        self.errors.append((ob.data, ob.data.animation_data.drivers,driver))
    def fix(self):
        for ob, drivers , driver in self.errors :
            drivers.remove(driver)    
    def get_fix_msg(self): 
        return 'Delete ' + str(len(self.errors)) + '  invalid drivers'  
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' -> ' + err[2].data_path     

class SC_VALID_FCURVES(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()     
        self.msg = 'Invalid Fcurve found : ' 
        self.fixable = True
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.animation_data and ob.animation_data.action :                    
                for fcurve in ob.animation_data.action.fcurves : 
                    if not valid_fcurve(ob, fcurve) :
                        self.errors.append(fcurve)         
            if ob.type == 'GPENCIL' and ob.data and ob.data.animation_data and ob.data.animation_data.action :                      
                ob.data.animation_data.drivers.update()
                for fcurve in ob.data.animation_data.action.fcurves :
                    if not valid_fcurve(ob.data, fcurve) :
                        self.errors.append(fcurve) 
    def fix(self):
        for fc in self.errors :
            fc.id_data.fcurves.remove(fc)    
    def get_fix_msg(self): 
        return 'Delete ' + str(len(self.errors)) + '  invalid fcurves'  
    def get_report_msg(self, err):
        return self.msg + err.id_data.name + ' -> ' + err.data_path     
    
class SC_GPENCIL_NO_MATERIAL_STROKE(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()     
        self.msg = 'Stroke with no material:  Object : ' 
    def check(self): 
        for ob in bpy.context.scene.objects :
            if ob.type == 'GPENCIL' and ob.name[:4] != 'CTLR':
                layers = []
                for layer in ob.data.layers :
                    frames = []
                    for frame in layer.frames :
                        for stroke in frame.strokes :
                            if stroke.material_index > len(ob.material_slots) -1 or ob.material_slots[stroke.material_index].material == None  :
                                frames.append(frame.frame_number)                     
                    if len(frames) > 0 :
                        layers.append((layer.info, frames))
                if len(layers) > 0 :
                    self.errors.append((ob, layers))      
    def get_report_msg(self, err):
        return self.msg + err[0].name + ' => ' + str(err[1])   
    
class SC_SCENE_COL_OBJECTS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = ' is linked directly to scene collection ' 
    def check(self): 
        for ob in bpy.context.scene.collection.objects :
            self.errors.append(ob.name)
    def get_report_msg(self, err):
        return'Object ' + err + self.msg
    
class SC_UNUSED_OBJECTS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = ' is not in linked the active scene and should be removed' 
        self.fixable = True
    def check(self): 
        for ob in bpy.data.objects :
            if ob not in list(bpy.context.scene.collection.all_objects) and not ob.library :
                self.errors.append(ob)
    def get_report_msg(self, err):
        return 'Object ' + err.name + self.msg 
    def fix(self):
        for ob in self.errors :
            bpy.data.objects.remove(ob)
    def get_fix_msg(self): 
        return 'Delete ' + str(len(self.errors)) + '  unused objects'  
    
class SC_KITSU_CONNECTED(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = ' /!!\ Kitsu not connected : ' 
    def check(self): 
        from ..common.kitsu_connection_checker import check_connection
        kitsu_connection = check_connection()
        if kitsu_connection != True :
            self.errors.append(kitsu_connection)
    def get_report_msg(self, err):
        return self.msg + err
    
class SC_CONFIRM_ASSET_NAME(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_KITSU_CONNECTED']   
        self.msg = 'Cant find the kitsu asset from filename, ( ' 
    def check(self): #TODO has to work on OUTPUT files
        filename = os.path.basename(bpy.data.filepath)
        filename_ok = False
        asset = None
        if not 'OUTPUT' in bpy.data.filepath :
            if len(filename.split('_')) >= 6 :
                projcode = filename.split('_')[0]
                ep = filename.split('_')[1]
                asset_type = filename.split('_')[2]
                asset_type_fullname = None       
                asset_name = '_'.join(filename.split('_')[3:-2])
                task = filename.split('_')[-2]
                version = filename.split('_')[-1]
                match asset_type :
                        case 'CH':
                            asset_type_fullname = 'CHARACTERS'
                        case 'PR':
                            asset_type_fullname = 'PROPS'
                        case 'CR':
                            asset_type_fullname = 'CREATURES'
                project = gazu.project.get_project_by_name("EWILAN")
                #episode = gazu.shot.get_episode_by_name(project, ep)
                asset_type_dic = gazu.asset.get_asset_type_by_name(asset_type_fullname)
                asset = gazu.asset.get_asset_by_name(project, asset_name, asset_type_dic)
                filename_ok = True
        if asset == None :
            self.errors.append('')
        if not filename_ok :
            self.errors.append('not solved')
    def get_report_msg(self, err):
        return self.msg + err + ' )'


class SC_CONFIRM_LAYOUT_SHOT(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_KITSU_CONNECTED']   
        self.msg = 'Cant find the kitsu shots from filename, ( ' 
    def check(self): 
        filename = os.path.basename(bpy.data.filepath)
        filename_ok = False
        #TODO change for regular expressions       
        if len(filename.split('_')) == 6 :
            proj_code, ep, sq, sh, task, worker = filename.split("_")
            filename_ok = True
        elif len(filename.split('_')) == 5 :
            proj_code, ep, sq, sh, task = filename.split("_")
            filename_ok = True   
        
        if filename_ok:
            project = gazu.project.get_project_by_name("EWILAN")
            episode = gazu.shot.get_episode_by_name(project, ep)
            sequence = gazu.shot.get_sequence_by_name(project,sq,episode)
            shot = gazu.shot.get_shot_by_name(sequence, sh)

        if shot == None :
            self.errors.append('not found')
        if not filename_ok :
            self.errors.append(f'filename not solved : {filename}')
    def get_report_msg(self, err):
        return self.msg + err + ' )'

class SC_CAMERA_DATANAME(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Camera data invald name ' 
        self.fixable = True
    def check(self): 
        for scene in bpy.data.scenes :
            for ob in scene.objects :
                if ob.type == 'CAMERA' :
                    if ob.data.users == 1 and ob.data.name != ob.name :
                        self.errors.append((ob.data, ob.name))
    def get_report_msg(self, err):
        return self.msg + err[0].name +  ' should be : ' + err[1]
    def fix(self):
        for cam, name in self.errors :
            cam.name = name
    def get_fix_msg(self): 
        return 'Rename ' + str(len(self.errors)) + '  camera datablocks'  
    
class SC_CAMERA_SETTINGS(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.parent_checks = ['SC_SCENE_UNIT_SCALE']
        self.msg = 'Anormal setting on Camera : '
    def check(self): 
        for cam in bpy.data.cameras :
            if cam.lens != 100.0:
                self.errors.append(cam.name + ' => Focal length should be 100')
            if round(cam.clip_start,5) != 1 :
                self.errors.append(cam.name + ' => clip start should be 0.1')
            if cam.users > 1 :
                self.errors.append(cam.name + ' => more than one user')
    def get_report_msg(self, err):
        return self.msg + err
    
class SC_NO_VERTEX_COLOR(SANITY_ELEMENT):
    def __init__(self):
        super().__init__()
        self.msg = 'Found vertex color use in object : ' 
        self.fixable = True
    def check(self): 
        for o in bpy.context.scene.objects :
            if o.type == 'GPENCIL' and not o.library :
                for layer in o.data.layers :
                    for frame in layer.frames :
                        for stroke in frame.strokes :
                            
                            if [i for i in stroke.vertex_color_fill] != [0,0,0,0] : 
                                if o not in self.errors :
                                    self.errors.append(o)
                                break
                            for p in stroke.points :
                                if [i for i in p.vertex_color] != [0,0,0,0] :
                                    if o not in self.errors :
                                        self.errors.append(o)
                                    break

    def get_report_msg(self, err):
        return self.msg + err.name 
    def fix(self):
        for o in self.errors :
            for layer in o.data.layers :
                for frame in layer.frames :
                    for stroke in frame.strokes :
                        stroke.vertex_color_fill = (0,0,0,0)
                        for p in stroke.points :
                            p.vertex_color = (0,0,0,0)
    def get_fix_msg(self): 
        return 'Remove vertex colors on ' + str(len(self.errors)) + '  objects'  



#TO ADD : disable rendering from vse and compositor