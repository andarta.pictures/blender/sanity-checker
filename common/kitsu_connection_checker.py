import bpy
'''connection checker from kitsu connection add-on - paste this file in the add-on repository '''
def check_connection():
    kitsu_connection = False
    preferences = bpy.context.preferences
    if 'kitsu-connection' in preferences.addons.keys():
        kitsu_prefs = preferences.addons['kitsu-connection'].preferences
        if kitsu_prefs.connected :
            kitsu_connection = True
    else : 
        return "you need to install/activate Kitsu connection add-on from andarta's repository"
    if not kitsu_connection :
        return "you're not connected to kitsu, check the kitsu panel"
    return kitsu_connection

'''
#template for connection checking to paste inside operators 

from .kitsu_connection_checker import check_connection
kitsu_connection = check_connection()
if kitsu_connection != True :
    self.report({'INFO'}, message = kitsu_connection)
    return {'CANCELLED'}
'''