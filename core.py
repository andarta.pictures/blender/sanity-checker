import bpy
import time
from .departments import departments

class SANITY_PROPS(bpy.types.PropertyGroup):
    bl_label = "sanity check property group"
    bl_idname = "sanitycheck_props"
    checkpoints = []
    items = []
    for i, k in enumerate(departments.keys()) :
        items.append((k,k,k,i))  
    department : bpy.props.EnumProperty(items = items)

class SANITY_OT_FIX(bpy.types.Operator):
    bl_idname = "sanity.fix"
    bl_label = " Sanity fixer"
    bl_options = {'UNDO'}
    index : bpy.props.IntProperty()
    def execute(self,context):
        cp = context.scene.sanity_check_props.checkpoints[self.index]
        cp.fix()
        cp.fixed = True
        return {'FINISHED'}

class SANITY_OT_CHECK(bpy.types.Operator):
    bl_idname = "sanity.check"
    bl_label = " Sanity checker"
    bl_options = {'UNDO'}
    department : bpy.props.StringProperty(default='TEST')
    debug = False
    def execute(self, context) :
        if self.department in departments.keys():
            self.checkpoints = departments[self.department] 
        else :
            print('unknown departement name')
            return {'CANCELLED'}
        
        context.scene.sanity_check_props.checkpoints.clear()    
        errors = []
        for checkpoint in self.checkpoints :
            if not self.debug :
                try :
                    cp = checkpoint()
                    if cp.parents_are_ok() :
                        
                        cp.check()
                        errs = cp.report()
                        for err in errs :
                            errors.append(err)
                        context.scene.sanity_check_props.checkpoints.append(cp)
                    else : 
                        errors.append('Cant perform checkpoint ' + cp.__class__.__name__  + ' due to an invalid parent checkpoint : ' + str(cp.parent_checks))
                except Exception as e :
                    errors.append('INTERNAL BUG /!\ Exception occured during the following checkpoint :' + checkpoint.__name__  + ' : ' + str(e))
            
            if self.debug : #avoid the try/except to help debugging
                cp = checkpoint()
                if cp.parents_are_ok() :
                    
                    cp.check()
                    errs = cp.report()
                    for err in errs :
                        errors.append(err)
                    context.scene.sanity_check_props.checkpoints.append(cp)
                else : 
                    errors.append('Cant perform checkpoint ' + cp.__class__.__name__  + ' due to an invalid parent checkpoint : ' + str(cp.parent_checks))
        
        self.write_sanity_report(errors)

        return {'FINISHED'}
    
    def write_sanity_report(self, errors) :
        if not 'Sanity Report' in bpy.data.texts.keys() : 
            report = bpy.data.texts.new('Sanity Report')
        else : 
            report = bpy.data.texts['Sanity Report']
            report.clear()
        report.write('-------------------' + self.department + ' Sanity check report : ----------' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + '\n')
        if len(errors) == 0 :
            report.write(' !!! CONGRATULATIONS ! No Errors in this file !!!\n \n')
        else : 
            for error in errors :
                report.write('/!\ ' + error + '\n')
        self.report({'INFO'} , 'Sanity check done, look at Sanity Report in text editor')

class SANITY_PT_PANEL(bpy.types.Panel):
   
    bl_label = "Sanity Check"
    bl_idname = "SANITY_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.prop(context.scene.sanity_check_props, 'department')
        row = layout.row()
        ops = row.operator('sanity.check')
        ops.department = context.scene.sanity_check_props.department

        if 'Sanity Report' in bpy.data.texts.keys() :
            report = bpy.data.texts['Sanity Report']
            row = layout.row()
            if  report.lines[1].body[:4] == ' !!!' :
                row.label(text = 'No errors !')
            else :
                row.label(text = str(len(report.lines)-2) + ' errors found : Look at Sanity Report in text editor')
            if len([cp for cp in context.scene.sanity_check_props.checkpoints if len(cp.errors) > 0 and cp.fixable == True and not cp.fixed]) > 0 :
                box = layout.box()
                for i, cp in enumerate(context.scene.sanity_check_props.checkpoints) :
                    if len(cp.errors) > 0 and cp.fixable == True and not cp.fixed:
                        row = box.row()
                        ops = row.operator('sanity.fix', text = cp.get_fix_msg())
                        ops.index = i