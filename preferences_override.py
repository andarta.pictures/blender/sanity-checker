# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from bpy.app.handlers import persistent


@persistent
def ensure_preferences(scene):
    log = []
    prefs = bpy.context.preferences

    if not prefs.filepaths.use_file_compression :
        prefs.filepaths.use_file_compression = True
        log.append("File compression activated.")
    
    if len(log) > 0 :
        print("\n### PREFERENCE OVERRIDE ###\n" + "\n".join(log)+"\n")

        
def register() :
    bpy.app.handlers.load_pre.append(ensure_preferences)
    bpy.app.handlers.save_pre.append(ensure_preferences)


def unregister() :
    bpy.app.handlers.save_pre.remove(ensure_preferences)
    bpy.app.handlers.load_pre.remove(ensure_preferences)