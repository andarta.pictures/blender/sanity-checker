# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Sanity checker",
    "author" : "Tom VIGUIER",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}
import bpy

from .core import *
from . import preferences_override

classes = [ SANITY_OT_CHECK ,
            SANITY_PROPS,
            SANITY_OT_FIX,
            SANITY_PT_PANEL      
            ]

def register():
    for cls in classes :
        bpy.utils.register_class(cls)  
    bpy.types.Scene.sanity_check_props = bpy.props.PointerProperty(type = SANITY_PROPS)
    preferences_override.register()


def unregister():
    preferences_override.unregister()
    for cls in classes :
        bpy.utils.unregister_class(cls)  
    del bpy.types.Scene.sanity_check_props
